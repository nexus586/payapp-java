package com.payapp.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * Created by nexus on 12/25/17.
 */
@Entity
@Table(name = "product")
public class Product {

    @Id
    @Column(name = "product_code")
    private String productCode;

    @NotNull
    @Column(name = "product_id")
    private long productId;

    private String productName;

    private String productDescription;

    private double amount;

    private String productImagePath;

    private String productDownloadLink;

    private String productUrl;

    private boolean showOnlyOnPurchase;

    @OneToMany
    private Collection<Invoice> invoices;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getProductImagePath() {
        return productImagePath;
    }

    public void setProductImagePath(String productImagePath) {
        this.productImagePath = productImagePath;
    }

    public String getProductDownloadLink() {
        return productDownloadLink;
    }

    public void setProductDownloadLink(String productDownloadLink) {
        this.productDownloadLink = productDownloadLink;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public boolean isShowOnlyOnPurchase() {
        return showOnlyOnPurchase;
    }

    public void setShowOnlyOnPurchase(boolean showOnlyOnPurchase) {
        this.showOnlyOnPurchase = showOnlyOnPurchase;
    }

    public Collection<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(Collection<Invoice> invoices) {
        this.invoices = invoices;
    }
}
