package com.payapp.models;

import com.payapp.shared.Account;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by nexus on 12/23/17.
 */
@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "transaction_id")
    private Long transactionId;

    @OneToOne
    private User parent;

    @OneToOne
    private User school;

    @OneToOne
    private Child child;

    @Column(name = "amount_payed")
    private double amount;

    @OneToOne
    private Account parentAccount;

    @OneToOne
    private Account schoolAccount;

    @Column(name = "date_of_payment")
    private Date transactionDate;

    @Transient
    private Long sId;

    @Transient
    private Long pAccountId;

    @Transient
    private Long cId;

    @Transient
    private Long sAccountId;


    public Long getsId() {
        return sId;
    }

    public void setsId(Long sId) {
        this.sId = sId;
    }

    public Long getpAccountId() {
        return pAccountId;
    }

    public void setpAccountId(Long pAccountId) {
        this.pAccountId = pAccountId;
    }

    public Long getcId() {
        return cId;
    }

    public void setcId(Long cId) {
        this.cId = cId;
    }

    public Long getsAccountId() {
        return sAccountId;
    }

    public void setsAccountId(Long sAccountId) {
        this.sAccountId = sAccountId;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public User getParent() {
        return parent;
    }

    public void setParent(User parent) {
        this.parent = parent;
    }

    public User getSchool() {
        return school;
    }

    public void setSchool(User school) {
        this.school = school;
    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Account getParentAccount() {
        return parentAccount;
    }

    public void setParentAccount(Account parentAccount) {
        this.parentAccount = parentAccount;
    }

    public Account getSchoolAccount() {
        return schoolAccount;
    }

    public void setSchoolAccount(Account schoolAccount) {
        this.schoolAccount = schoolAccount;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }
}
