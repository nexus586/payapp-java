package com.payapp.hubtel;

/**
 * Created by nexus on 12/31/17.
 */
public class SendMoMoRequestResponse {
    private String responseCode;
    private Data data;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
