package com.payapp.hubtel;

/**
 * Created by nexus on 12/31/17.
 */
public class ReceiveMoMoRequest {
    private String customerName;
    private String customerEmail;
    private String customerMsisdn;
    private String channel;
    private double amount;
    private String primaryCallbackURL;
    private String secondaryCallbackURL;
    private String clientReference;
    private String description;
    private String token;
    private boolean feesOnCustomer;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerMsisdn() {
        return customerMsisdn;
    }

    public void setCustomerMsisdn(String customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPrimaryCallbackURL() {
        return primaryCallbackURL;
    }

    public void setPrimaryCallbackURL(String primaryCallbackURL) {
        this.primaryCallbackURL = primaryCallbackURL;
    }

    public String getSecondaryCallbackURL() {
        return secondaryCallbackURL;
    }

    public void setSecondaryCallbackURL(String secondaryCallbackURL) {
        this.secondaryCallbackURL = secondaryCallbackURL;
    }

    public String getClientReference() {
        return clientReference;
    }

    public void setClientReference(String clientReference) {
        this.clientReference = clientReference;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isFeesOnCustomer() {
        return feesOnCustomer;
    }

    public void setFeesOnCustomer(boolean feesOnCustomer) {
        this.feesOnCustomer = feesOnCustomer;
    }
}
