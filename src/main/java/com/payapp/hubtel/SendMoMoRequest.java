package com.payapp.hubtel;

/**
 * Created by nexus on 12/31/17.
 */
public class SendMoMoRequest {
    private String recipientName;
    private String recipientMsisdn;
    private String customerEmail;
    private String channel;
    private double amount;
    private String primaryCallbackURL;
    private String secondaryCallbackURL;
    private String description;
    private String clientResponse;

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientMsisdn() {
        return recipientMsisdn;
    }

    public void setRecipientMsisdn(String recipientMsisdn) {
        this.recipientMsisdn = recipientMsisdn;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPrimaryCallbackURL() {
        return primaryCallbackURL;
    }

    public void setPrimaryCallbackURL(String primaryCallbackURL) {
        this.primaryCallbackURL = primaryCallbackURL;
    }

    public String getSecondaryCallbackURL() {
        return secondaryCallbackURL;
    }

    public void setSecondaryCallbackURL(String secondaryCallbackURL) {
        this.secondaryCallbackURL = secondaryCallbackURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClientResponse() {
        return clientResponse;
    }

    public void setClientResponse(String clientResponse) {
        this.clientResponse = clientResponse;
    }
}
