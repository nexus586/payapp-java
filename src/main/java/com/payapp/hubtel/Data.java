package com.payapp.hubtel;

/**
 * Created by nexus on 12/31/17.
 */
public class Data {
    private double amountAfterCharges;
    private String transactionId;
    private String clientResponse;
    private String description;
    private String externalTransactionId;
    private double amount;
    private double charges;

    public double getAmountAfterCharges() {
        return amountAfterCharges;
    }

    public void setAmountAfterCharges(double amountAfterCharges) {
        this.amountAfterCharges = amountAfterCharges;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getClientResponse() {
        return clientResponse;
    }

    public void setClientResponse(String clientResponse) {
        this.clientResponse = clientResponse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExternalTransactionId() {
        return externalTransactionId;
    }

    public void setExternalTransactionId(String externalTransactionId) {
        this.externalTransactionId = externalTransactionId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getCharges() {
        return charges;
    }

    public void setCharges(double charges) {
        this.charges = charges;
    }
}
