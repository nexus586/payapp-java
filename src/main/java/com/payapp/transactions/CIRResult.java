package com.payapp.transactions;

import java.util.Collection;

/**
 * Created by nexus on 12/24/17.
 */
public class CIRResult {
    private String orderCode;
    private String paymentCode;
    private String payToken;
    private String description;
    private String qrCodeUrl;
    private double fullDiscountAmount;
    private Collection<Object> discounts;

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getPayToken() {
        return payToken;
    }

    public void setPayToken(String payToken) {
        this.payToken = payToken;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQrCodeUrl() {
        return qrCodeUrl;
    }

    public void setQrCodeUrl(String qrCodeUrl) {
        this.qrCodeUrl = qrCodeUrl;
    }

    public double getFullDiscountAmount() {
        return fullDiscountAmount;
    }

    public void setFullDiscountAmount(double fullDiscountAmount) {
        this.fullDiscountAmount = fullDiscountAmount;
    }

    public Collection<Object> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(Collection<Object> discounts) {
        this.discounts = discounts;
    }
}
