package com.payapp.transactions;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by nexus on 12/24/17.
 */
//@Configuration
@PropertySource("classpath:application.properties")
public class ListPayOptionsRequest {

    @Value("${api.merchant.email}")
    private String emailOrMobileNumber;

    @Value("${api.merchant.key}")
    private String merchantKey;

    public String getEmailOrMobileNumber() {
        return emailOrMobileNumber;
    }

    public void setEmailOrMobileNumber(String emailOrMobileNumber) {
        this.emailOrMobileNumber = emailOrMobileNumber;
    }

    public String getMerchantKey() {
        return merchantKey;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }
}
