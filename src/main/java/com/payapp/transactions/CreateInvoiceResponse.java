package com.payapp.transactions;

/**
 * Created by nexus on 12/24/17.
 */
public class CreateInvoiceResponse {
    private boolean success;
    private CIRResult result;
    private String errorMessage;
    private String errorCode;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public CIRResult getResult() {
        return result;
    }

    public void setResult(CIRResult result) {
        this.result = result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
