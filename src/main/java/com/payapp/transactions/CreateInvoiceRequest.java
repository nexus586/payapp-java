package com.payapp.transactions;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import java.util.Collection;

/**
 * Created by nexus on 12/24/17.
 */
@PropertySource("classpath:application.properties")
public class CreateInvoiceRequest {

    @Value("${api.merchant.email}")
    private String emailOrMobileNumber;

    @Value("${api.merchant.key}")
    private String merchantKey;
    private double amount;
    private String orderCode;
    private Collection<OrderingItem> orderItems;
    private boolean sendInvoice;
    private String payOption;
    private String customerName;
    private String customerEmail;
    private String customerMobileNumber;

    public String getEmailOrMobileNumber() {
        return emailOrMobileNumber;
    }

    public void setEmailOrMobileNumber(String emailOrMobileNumber) {
        this.emailOrMobileNumber = emailOrMobileNumber;
    }

    public String getMerchantKey() {
        return merchantKey;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public Collection<OrderingItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Collection<OrderingItem> orderItems) {
        this.orderItems = orderItems;
    }

    public boolean isSendInvoice() {
        return sendInvoice;
    }

    public void setSendInvoice(boolean sendInvoice) {
        this.sendInvoice = sendInvoice;
    }

    public String getPayOption() {
        return payOption;
    }

    public void setPayOption(String payOption) {
        this.payOption = payOption;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }
}
