package com.payapp.auth.details;

import com.payapp.models.User;
import com.payapp.repositories.UserRepository;
import com.payapp.shared.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by nexus on 2/16/18.
 */
@Service
public class JwtUserDetailsImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);

        if(user == null){
            throw new UsernameNotFoundException("No user found with email: "+email.toString());
        }else{
            return new JWTUser(
                    user.getId(),
                    user.getEmail(),
                    user.getName(),
                    user.getPassword(),
                    mapToGrantedAuthorities(user.getRoles()),
                    user.isEnabled()
            );
        }
    }

    private Collection<GrantedAuthority> mapToGrantedAuthorities(Collection<Role> roles){
        return roles.stream()
                    .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                    .collect(Collectors.toList());
    }
}
