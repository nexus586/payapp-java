package com.payapp.mail;

import freemarker.template.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.util.Map;

/**
 * Created by nexus on 1/6/18.
 */
@Component
public class FreeMarker {

    private Configuration freeMarkerTemplateConfiguration;

    public FreeMarker(Configuration freeMarkerTemplateConfiguration){
        this.freeMarkerTemplateConfiguration = freeMarkerTemplateConfiguration;
    }

    /*
     * FreeMarker configuration.
     * Sets the location of the templates
     * This is used by the framework
     */

    @Bean
    public FreeMarkerConfigurationFactoryBean getFreeMarkerConfiguration() {
        //get a new bean
        FreeMarkerConfigurationFactoryBean fmConfigFactoryBean = new FreeMarkerConfigurationFactoryBean();
        //set the template location
        fmConfigFactoryBean.setTemplateLoaderPath("/templates/");
        //return the bean
        return fmConfigFactoryBean;
    }

    //processes the data model and the html page
    public String processDataModel(Map<String, Object> model, String fileName){
        //thread safe
        StringBuffer content = new StringBuffer();
        try{
            //append the processed html
            content.append(FreeMarkerTemplateUtils.processTemplateIntoString(
                    freeMarkerTemplateConfiguration.getTemplate(fileName),model));
            //return on success
            return content.toString();
        }catch(Exception e){
            System.out.println("Exception occurred while processing the template: "+e.getMessage());
        }
        //return nothing on success
        return "";
    }


}
