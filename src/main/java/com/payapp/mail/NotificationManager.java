package com.payapp.mail;

import com.payapp.models.User;

/**
 * Created by nexus on 1/6/18.
 */
public interface NotificationManager {

    void sendNotification(User s);
    void sendAccountNotification(User p);
    void sendAccountCreationNotification(User u);

}
