package com.payapp.mail;

import com.payapp.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by nexus on 1/6/18.
 */
@Service
public class MailManager implements NotificationManager {
    @Value("${spring.mail.username}")
    private String senderEmail;

    @Value("${spring.mail.password}")
    private String senderPassword;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private FreeMarker freeMarker;

    private String message;

    public MailSender getMailSender(){
        return mailSender;
    }

    public void setMailSender(JavaMailSender mailSender){
        this.mailSender = mailSender;
    }

    private String getMessage(){
        return this.message;
    }

    public void setMessage(String message){
        this.message = message;
    }

    @Bean
    public JavaMailSender getJavaMailSender(){
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername(senderEmail);
        mailSender.setPassword(senderPassword);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol","smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    @Override
    public void sendAccountCreationNotification(User p) throws MailException{
        try{
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(p.getEmail());
            helper.setFrom("no-reply@payapp.com");
            helper.setSubject("Email Confirmation");
            Map<String, Object> dataModel = new HashMap<>();
            dataModel.put("user", p);
            dataModel.put("message", this.getMessage());
            helper.setText(freeMarker.processDataModel(dataModel, "reg_email.ftl"), true);
            this.mailSender.send(message);
        }catch (MessagingException me){
            System.out.println("message not sent: "+me.getMessage());
        }
    }

    @Override
    public void sendNotification(User p) throws MailException{

    }

    @Override
    public void sendAccountNotification(User p) throws MailException{
        try{
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(p.getEmail());
            helper.setFrom("no-reply@payapp.com");
            helper.setSubject("Account Creation");
            Map<String, Object> dataModel = new HashMap<>();
            dataModel.put("user", p);
            dataModel.put("message", this.getMessage());
            helper.setText(freeMarker.processDataModel(dataModel, "confirm.ftl"), true);
            this.mailSender.send(message);
        }catch (MessagingException me){
            System.out.println("message not sent: "+me.getMessage());
        }
    }

}
