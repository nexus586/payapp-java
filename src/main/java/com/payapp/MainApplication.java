package com.payapp;

import com.payapp.repositories.RoleRepository;
import com.payapp.shared.Role;
import com.payapp.shared.RoleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collection;

import static springfox.documentation.builders.PathSelectors.regex;

@SpringBootApplication
@EnableSwagger2
@EnableAsync
@PropertySource("classpath:application.properties")
public class MainApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

	@Autowired
	private RoleRepository roleRepository;

	@Bean
	public Docket newsApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("PayApp")
				.apiInfo(apiInfo())
				.select()
				.paths(regex("/.*"))
				.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("PayApp")
				.description("An app to reduce the hustle of paying fees")
				.termsOfServiceUrl("http://cannotcontainallthisawesomeness.com")
				.contact("danagbemava@gmail.com")
				.license("Apache License Version 7.0")
				.licenseUrl("http://cannotcontainallthisawesomeness.com/LICENSE")
				.version("7.0")
				.build();

	}

	@Override
	public void run(String... args) throws Exception {
		Collection<Role> roles = roleRepository.findAll();

		if(roles.toArray().length == 0){
			Role parentRole = new Role();
			parentRole.setName(RoleType.ROLE_PARENT);
			roleRepository.save(parentRole);

			Role adminRole = new Role();
			adminRole.setName(RoleType.ROLE_ADMIN);
			roleRepository.save(adminRole);


			Role schoolRole = new Role();
			schoolRole.setName(RoleType.ROLE_SCHOOL);
			roleRepository.save(schoolRole);

			Role userRole = new Role();
			userRole.setName(RoleType.ROLE_USER);
			roleRepository.save(userRole);
		}
	}
}
