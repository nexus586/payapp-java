package com.payapp.repositories;

import com.payapp.models.Child;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

/**
 * Created by nexus on 12/17/17.
 */
public interface ChildRepository extends JpaRepository<Child, Long> {
    Collection<Child> findChildrenByParentEmail(String parentEmail);
    Child findByLastNameAndParentId(String childLastName, Long id);
    Collection<Child> findAllByParentId(Long id);
    Child findByFirstNameAndLastNameAndParentId(String firstName, String lastName, Long parentId);
}
