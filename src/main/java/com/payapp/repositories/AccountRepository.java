package com.payapp.repositories;

import com.payapp.shared.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

/**
 * Created by nexus on 2/16/18.
 */
public interface AccountRepository extends JpaRepository<Account, Long>{
    Account findByAccountNumber(String accountNumber);
    Account findByUserEmail(String email);
    Collection<Account> findAllByUserEmail(String email);
    Collection<Account> findAllByUserId(Long id);
    Account findByAccountId(Long id);
}
