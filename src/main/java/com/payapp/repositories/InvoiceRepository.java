package com.payapp.repositories;

import com.payapp.models.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by nexus on 12/25/17.
 */
public interface InvoiceRepository extends JpaRepository<Invoice, String> {
    Invoice findByPayToken(String payToken);
    Invoice findByOrderCode(String orderCode);
}
