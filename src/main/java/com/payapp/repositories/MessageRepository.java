package com.payapp.repositories;

import com.payapp.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;

//import java.util.Collection;

/**
 * Created by nexus on 3/18/18.
 */
public interface MessageRepository extends JpaRepository<Message, Long> {
}
