package com.payapp.repositories;

import com.payapp.models.User;
import com.payapp.shared.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

/**
 * Created by nexus on 2/7/18.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByName(String name);
    User findByEmail(String email);
    Collection<User> findAllByRoles(Role role);
}
