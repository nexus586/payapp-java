package com.payapp.repositories;

import com.payapp.models.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by nexus on 12/25/17.
 */
public interface OrderItemRepository extends JpaRepository<OrderItem, String> {
}
