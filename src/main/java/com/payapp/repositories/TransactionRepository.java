package com.payapp.repositories;

import com.payapp.models.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

/**
 * Created by nexus on 12/23/17.
 */
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Collection<Transaction> findAllByParentId(Long id);
    Collection<Transaction> findAllBySchoolId(Long schoolId);
}
