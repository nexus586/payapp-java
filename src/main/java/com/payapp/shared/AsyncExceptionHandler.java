package com.payapp.shared;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import java.lang.reflect.Method;

/**
 * Created by nexus on 1/5/18.
 */
public class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
    @Override
    public void handleUncaughtException(Throwable ex, Method method, Object... params) {
        System.out.println("Exception Message: "+ex.getMessage());
        System.out.println("Method name: "+method.getName());

        for(Object param: params){
            System.out.println("Param value: "+param);
        }
    }
}
