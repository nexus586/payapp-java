package com.payapp.shared;

/**
 * Created by nexus on 12/17/17.
 */
public enum RoleType {
    ROLE_PARENT, ROLE_SCHOOL, ROLE_ADMIN, ROLE_USER
}
