package com.payapp.shared;

import org.springframework.scheduling.annotation.Async;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

/**
 * Created by nexus on 12/25/17.
 */
public class HttpWrapper {
    public enum HttpMethods {
        GET, POST, DELETE, PUT, PATCH
    }

    private static final String CLIENT_ID = "pocmhafr";
    private static final String CLIENT_SECRET = "xqtcwxmj";

    private static final String AUTH = "Basic "+
            Base64.getEncoder().encodeToString((CLIENT_ID+":"+CLIENT_SECRET).getBytes());

    public enum HttpContentType {
        ApplicationJson("application/json"), ApplicationXml("text/xml"), PlainText("plain/text");

        private final String text;

        private HttpContentType(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public String MakeHubtelCalls(String httpUrl, HttpMethods method, HttpContentType contentType, String postData) {
        String responseText = null;
        try {
            URL url = new URL(httpUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method.toString());
            connection.setRequestProperty("Content-Type", contentType.toString());
            connection.setRequestProperty("Authorization", AUTH);
            if (method == HttpMethods.POST) {
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setAllowUserInteraction(false);
            }

            if (method != HttpMethods.GET && postData != null) {
                connection.setRequestProperty("Content-Length", Integer.toString(postData.getBytes().length));
                try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                    wr.writeBytes(postData);
                }

            }

            int responseCode = connection.getResponseCode();
            System.out.println("Response code is " + responseCode);

            if (responseCode == 200) {

                try (BufferedReader resp = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                    String response;
                    StringBuilder builder = new StringBuilder();
                    while ((response = resp.readLine()) != null) {
                        builder.append(response);
                    }
                    responseText = builder.toString();
                }
            }

            connection.disconnect();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return responseText;
    }

    @Async("threadPoolTaskExecutor")
    public String MakeHttpCalls(String httpUrl, HttpMethods method, HttpContentType contentType, String postData) {
        String responseText = null;
        try {
            URL url = new URL(httpUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method.toString());
            connection.setRequestProperty("Content-Type", contentType.toString());

            if (method == HttpMethods.POST) {
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setAllowUserInteraction(false);
            }

            if (method != HttpMethods.GET && postData != null) {
                connection.setRequestProperty("Content-Length", Integer.toString(postData.getBytes().length));
                try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                    wr.writeBytes(postData);
                }

            }

            int responseCode = connection.getResponseCode();
            System.out.println("Response code is " + responseCode);

            if (responseCode == 200) {

                try (BufferedReader resp = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                    String response;
                    StringBuilder builder = new StringBuilder();
                    while ((response = resp.readLine()) != null) {
                        builder.append(response);
                    }
                    responseText = builder.toString();
                }
            }

            connection.disconnect();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return responseText;
    }
}
