package com.payapp.controllers;

import com.google.gson.Gson;
import com.payapp.auth.details.JWTUser;
import com.payapp.auth.jwt.JwtTokenHelper;
import com.payapp.models.Login;
import com.payapp.models.Transaction;
import com.payapp.models.User;
import com.payapp.repositories.*;
import com.payapp.shared.Account;
import com.payapp.shared.AccountType;
import com.payapp.shared.Role;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nexus on 2/16/18.
 */
@RestController
@RequestMapping("/payapp/user/")
public class UserController {

    private UserRepository userRepository;
    private AccountRepository accountRepository;
    private BCryptPasswordEncoder passwordEncoder;
    private ChildRepository childRepository;
    private JwtTokenHelper helper;
    private AuthenticationManager manager;
    private UserDetailsService userDetailsService;
    private RoleRepository roleRepository;
    private TransactionRepository transactionRepository;

    public UserController(UserRepository userRepository,
                          AccountRepository accountRepository,
                          BCryptPasswordEncoder passwordEncoder,
                          ChildRepository childRepository,
                          JwtTokenHelper helper,
                          AuthenticationManager manager,
                          UserDetailsService userDetailsService,
                          RoleRepository roleRepository,
                          TransactionRepository transactionRepository){
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
        this.childRepository = childRepository;
        this.helper = helper;
        this.manager = manager;
        this.userDetailsService = userDetailsService;
        this.roleRepository = roleRepository;
        this.transactionRepository = transactionRepository;
    }


    /***********************************************************************
    *                           POST REQUESTS
     *********************************************************************/


    /**
     * Authenticates the user and generates a token for the user
     * @param user: A user sends in their email and password and they're authenticated
     * @return generated token, user_roles and user_id
     */
    @PostMapping(value = "login")
    public ResponseEntity<?> login(@RequestBody Login user){
        Map<String, Object> response = new HashMap<>();

        final Authentication authentication = manager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);


        final JWTUser userDetails = (JWTUser) userDetailsService.loadUserByUsername(user.getEmail());
        final String token = helper.generateToken(userDetails);

        response.put("token", token);
        response.put("roles", userDetails.getAuthorities());
        response.put("user", userDetails.getId());
        response.put("name", userDetails.getName());

        return ResponseEntity.ok(response);
    }


    @PostMapping(value = "forgotPasswordIndex")
    public ResponseEntity<?> forgotPasswordIndex(@RequestBody User email){
        Map<String, Object> response = new HashMap<>();

        System.out.println("EMail is: " + email.getEmail());

        if(userRepository.findByEmail(email.getEmail()) == null){
            response.put("Status", "Error");
            response.put("Reason", "Email not found or invalid email");
            return ResponseEntity.badRequest().body(response);
        }

        response.put("email", email.getEmail());
        return ResponseEntity.ok(response);
    }

    @PostMapping(value = "changePassword")
    public ResponseEntity<?> changePassword(@RequestBody User user){
        Map<String, String> response = new HashMap<>();
        User mUser = userRepository.findByEmail(user.getEmail());

        if(mUser == null){
            response.put("status", "error");
            response.put("reason", "account was not found");
            return ResponseEntity.badRequest().body(response);
        }

        try{
            mUser.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.save(mUser);
        }catch (Exception e){
            response.put("status", "Error");
            response.put("reason", "An error occurred");
            return ResponseEntity.badRequest().body(response);
        }

        response.put("status", "success");
        response.put("reason", "Password change successfully");

        return ResponseEntity.ok(response);
    }

    /**
     * Creates a new payment account for the user
     * @param account: an account object
     * @param id: the user's id
     * @return success or failure depending on whether account creation was successful or not
     */
    @PostMapping(value = "addAccount")
    public ResponseEntity<?> addAccount(@RequestBody Account account, @RequestParam(name = "id") Long id){

        //looks for the user in the database
        User user = userRepository.findOne(id);
        HashMap<String, String> response = new HashMap<>();



        //return failure if user is not found
        if(user == null){
            response.put("status", "error");
            response.put("reason", "Your account was not found");
            return ResponseEntity.badRequest().body(response);
        }


        //looks for the account in the database
        Account mAccount = accountRepository.findByAccountNumber(account.getAccountNumber());

        //if account is not found
        if(mAccount == null){

            mAccount = new Account();
            mAccount.setUser(user);
            mAccount.setAccountName(account.getAccountName());
            mAccount.setAccountNumber("0"+account.getAccountNumber());
            switch (account.getAccountProvider()){
                case "mtn":
                case "MTN":
                case "Mtn":
                    mAccount.setAccountType(AccountType.MTN_MOBILE_MONEY);
                    mAccount.setChannel("mtn-gh");
                    break;
                case "vodafone":
                case "Vodafone":
                case "VODAFONE":
                    mAccount.setAccountType(AccountType.VODAFONE_CASH);
                    mAccount.setChannel("vodafone-gh");
                    break;
                case "tigo":
                case "tiGO":
                case "TIGO":
                    mAccount.setAccountType(AccountType.TIGO_CASH);
                    mAccount.setChannel("tigo-gh");
                    break;
                case "airtel":
                case "Airtel":
                case "AIRTEL":
                    mAccount.setAccountType(AccountType.AIRTEL_MONEY);
                    mAccount.setChannel("airtel-gh");
                    break;
                case "visa":
                case "VISA":
                    mAccount.setAccountType(AccountType.VISA);
                    break;
                default:
                    response.put("status", "Failure");
                    response.put("reason", "Your account type is not yet supported");
                    return ResponseEntity.badRequest().body(response);
                }

            try{
                accountRepository.save(mAccount);
                response.put("status", "Success");
                response.put("reason", "Account Successfully added");
            }catch (Exception e){
                response.put("status", "Failure");
                response.put("reason", "An error occurred while trying to add your account. Please try again later");
                return ResponseEntity.badRequest().body(response);
            }
        }else{
            response.put("status", "Error");
            response.put("reason", "Your account already exists");
            return ResponseEntity.badRequest().body(response);
        }


        return ResponseEntity.ok(response);
    }


    /***********************************************************************
     *                           GET REQUESTS
     *********************************************************************/

    /**
     * Returns a user
     * @param id: The id the user was assigned upon account creation
     * @return the user's details
     */

    @GetMapping(value = "getUserById")
    public User getUserById(@RequestParam(name = "id")Long id){
        if(userRepository.findOne(id) == null){
            return null;
        }

        return userRepository.findOne(id);
    }

    /**
     * Returns all a user's accounts
     * @param id: the user_id is passed so the user's accounts can be found
     * @return Collection<Accounts> accounts or null if account no accounts are found
     */

    @GetMapping(value = "getAllAccounts")
    public Collection<Account> getAllAccounts(@RequestParam(name = "id")Long id){
        return accountRepository.findAllByUserId(id);
    }

    /**
     * Returns a particular account by it's id
     * @param id: the accountId is passed from the application to retrieve the account.
     * @return <p>Account<p/> or <p>null<p/> if account is not found
     */

    @GetMapping(value = "getAccountById")
    public Account getAccountById(@RequestParam(name = "id")Long id){
        if(accountRepository.findOne(id) == null){
            return null;
        }

        return accountRepository.findOne(id);
    }

    @GetMapping(value = "viewTransactionById")
    public Transaction viewTransactionById(@RequestParam(name = "id")Long id){
        if(transactionRepository.findOne(id) == null){
            return null;
        }
        return transactionRepository.findOne(id);
    }

    @GetMapping(value = "getAllSchools")
    public Collection<User> getAllSchools(){
        Role schoolRole = roleRepository.findById(3l);
        return userRepository.findAllByRoles(schoolRole);
    }

    /***********************************************************************
     *                           PUT REQUESTS
     *********************************************************************/


    @PutMapping(value = "editUser")
    public ResponseEntity<?> editUser(@RequestParam(name = "id") Long userID, @RequestBody User user){
        HashMap<String, String> response = new HashMap<>();
        User mUser = userRepository.findOne(userID);


        if(mUser == null){
            response.put("status", "Error");
            response.put("reason", "Account was not found");
            return ResponseEntity.badRequest().body(response);
        }

        try{
            mUser.setName(user.getName());
            mUser.setEmail(user.getEmail());
            mUser.setPhone(user.getPhone());
            userRepository.save(mUser);
            response.put("status", "Success");
            response.put("reason", "Changes saved");
        }catch (Exception e){
            response.put("status", "Error");
            response.put("reason", "Unable to save changes");
            return ResponseEntity.badRequest().body(response);
        }

        return ResponseEntity.ok(response);

    }

    @PutMapping(value = "editAccount")
    public ResponseEntity<?> editAccount(@RequestParam(name = "id")Long accountID, @RequestBody Account account){
        Account sAccount = accountRepository.findOne(accountID);
        HashMap<String, String> response = new HashMap<>();


        if(sAccount == null){
            response.put("status", "Error");
            response.put("reason", "Account not found");
            return ResponseEntity.badRequest().body(response);
        }else {
//            sAccount.setAccountType(sAccount.getAccountType());
            sAccount.setAccountNumber(account.getAccountNumber());
            sAccount.setAccountName(account.getAccountName());
            switch (account.getAccountProvider()){
                case "mtn":
                case "MTN":
                case "Mtn":
                    sAccount.setAccountType(AccountType.MTN_MOBILE_MONEY);
                    sAccount.setChannel("mtn-gh");
                    break;
                case "vodafone":
                case "Vodafone":
                case "VODAFONE":
                    sAccount.setAccountType(AccountType.VODAFONE_CASH);
                    sAccount.setChannel("vodafone-gh");
                    break;
                case "tigo":
                case "tiGO":
                case "TIGO":
                    sAccount.setAccountType(AccountType.TIGO_CASH);
                    sAccount.setChannel("tigo-gh");
                    break;
                case "airtel":
                case "Airtel":
                case "AIRTEL":
                    sAccount.setAccountType(AccountType.AIRTEL_MONEY);
                    sAccount.setChannel("airtel-gh");
                    break;
                case "visa":
                case "VISA":
                    sAccount.setAccountType(AccountType.VISA);
                    break;
                default:
                    response.put("status", "Failure");
                    response.put("reason", "Your account type is not yet supported");
                    return ResponseEntity.badRequest().body(response);
            }

            try{
                accountRepository.save(sAccount);
                response.put("status", "Success");
                response.put("reason", "Changes Saved");
            }catch (Exception e){
                response.put("status", "Error");
                response.put("reason", "Unable to save changes. Please try again later");
                return ResponseEntity.badRequest().body(response);
            }
        }

        return ResponseEntity.ok(response);

    }

    /***********************************************************************
     *                           DELETE REQUESTS
     *********************************************************************/
    @DeleteMapping(value = "removeAccount")
    public ResponseEntity<?> removeAccount(@RequestParam(name = "id") Long id){
        HashMap<String, String> response = new HashMap<>();
        try{
            accountRepository.delete(id);
            response.put("status", "Success");
            response.put("reason", "Account removed successfully");
        }catch (Exception e){
            response.put("status", "Error");
            response.put("reason", "Unable to remove account. Please try again later");
            return ResponseEntity.badRequest().body(response);
        }

        return ResponseEntity.ok(response);
    }

}
