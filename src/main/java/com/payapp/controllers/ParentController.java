package com.payapp.controllers;

import com.google.gson.Gson;
import com.payapp.auth.details.JWTUser;
import com.payapp.auth.jwt.JwtTokenHelper;
import com.payapp.hubtel.Data;
import com.payapp.hubtel.ReceiveMoMoRequest;
import com.payapp.hubtel.ReceiveMoMoRequestResponse;
import com.payapp.mail.MailManager;
import com.payapp.models.*;
import com.payapp.repositories.*;
import com.payapp.shared.*;
import java.sql.Date;
import com.payapp.transactions.*;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by nexus on 12/8/17.
 */
@Controller
@RequestMapping("/payapp/parent")
public class ParentController {

    private RoleRepository roleRepository;
    private ChildRepository childRepository;
    private TransactionRepository transactionRepository;
    private PasswordEncoder encoder;
    private MailManager mailManager;
    private UserRepository userRepository;
    private AccountRepository accountRepository;
    private JwtTokenHelper helper;
    private UserDetailsService userDetailsService;

    public ParentController(
                            RoleRepository roleRepository, ChildRepository childRepository,
                            PasswordEncoder encoder,
                            TransactionRepository transactionRepository,
                            MailManager mailManager,
                            UserRepository userRepository,
                            AccountRepository accountRepository,
                            JwtTokenHelper helper,
                            UserDetailsService userDetailsService){
        this.roleRepository = roleRepository;
        this.childRepository = childRepository;
        this.encoder = encoder;
        this.transactionRepository = transactionRepository;
        this.mailManager = mailManager;
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.helper = helper;
        this.userDetailsService = userDetailsService;
    }

    /******************************************************
     *                  POST REQUESTS
     *****************************************************/
    @PostMapping(value = "/signUp")
    public ResponseEntity<?> signUp(@RequestBody User parent){
        User p = userRepository.findByEmail(parent.getEmail());
        Collection<Role> r = new ArrayList<>();


        Map<String, Object> response = new HashMap<>();

        if(p == null){
//            try{
//                mailManager.setMessage("We are sending this email to confirm that your email is real " +
//                        "before we create your account");
//                mailManager.sendAccountCreationNotification(parent);
//            }catch (Exception e){
//                response.put("status", "error");
//                response.put("reason", "We were unable to confirm your email.Please try again later");
//                return ResponseEntity.badRequest().body(response);
//            }
            Role userRole = roleRepository.findById(4l);
            Role parentRole = roleRepository.findById(1l);

//            r.add(userRole);
            r.add(parentRole);
            p = new User();
            p.setName(parent.getName());
            p.setEmail(parent.getEmail());
            p.setPassword(encoder.encode(parent.getPassword()));
            p.setPhone("0"+parent.getPhone());
            p.setRoles(r);
            p.setEnabled(true);
            p.setLoggedIn(true);
            userRepository.save(p);
            response.put("status", "Success");
            response.put("reason", "Account Created Successfully");
            response.put("user", p);
        }else {
            response.put("status", "Error");
            response.put("reason", "You already have an account\nPlease Log In");
            return ResponseEntity.badRequest().body(response);
        }

        try{
            final JWTUser retUser = (JWTUser) userDetailsService.loadUserByUsername(p.getEmail());
            final String token = helper.generateToken(retUser);
            response.put("token", token);
            response.put("user", retUser.getId());
            response.put("name", retUser.getName());
            response.put("roles", retUser.getAuthorities());
        }catch (Exception e){
           response.put("status", "Error");
           response.put("reason", "Sorry An Unexpected Error Occurred");
        }


//        try{
//            mailManager.setMessage("Account has been successfully created. You can now use the full features of PayApp. " +
//                    "Thank you for signing up with us");
//            mailManager.sendAccountNotification(parent);
//        }catch (Exception e){
//            response.put("status", "error");
//            response.put("reason", "Something bad happened");
//            return ResponseEntity.badRequest().body(response);
//        }

        return ResponseEntity.ok(response);
    }


    @PostMapping("/addNewChild")
    public ResponseEntity<?> addNewChild(@RequestBody Child child, @RequestParam Long id){
        Map<String, String> response = new HashMap<>();

        User p = userRepository.findOne(id);

        Child c = childRepository.findByFirstNameAndLastNameAndParentId(child.getFirstName(),child.getLastName(), p.getId());


        if(c == null){

            c = new Child();
            c.setDateOfBirth(Date.valueOf(child.getDobString()));
            c.setFirstName(child.getFirstName());
            c.setLastName(child.getLastName());
            c.setParent(p);
            c.setSchoolName(child.getSchoolName());
            c.setGrade(child.getGrade());
            c.setAge(new Date(new java.util.Date().getTime()).toLocalDate().getYear() - c.getDateOfBirth().toLocalDate().getYear());
            c.setSchool(userRepository.findByName(child.getSchoolName()));
            try{
                childRepository.save(c);
                response.put("status", "success");
                response.put("reason", "You have successfully added a child");
                return ResponseEntity.ok(response);
            }catch (Exception e){
                response.put("status", "error");
                response.put("reason", "Unable to add child\nPlease try again");
                return ResponseEntity.badRequest().body(response);
            }

        }else{
            response.put("status", "error");
            response.put("reason", "Your child is already in our records");
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/receiveMoMoPayment")
    public ResponseEntity<?> receiveMoMoPayment(@RequestParam(name = "id")Long id, @RequestBody Transaction trans){
        Map<String, String> returnResponse = new HashMap<>();
        ReceiveMoMoRequestResponse requestResponse = new ReceiveMoMoRequestResponse();


        User parent = userRepository.findOne(id);
        Account parentAccount = accountRepository.findOne(trans.getpAccountId());
        Child child = childRepository.findOne(trans.getcId());

        Transaction transaction = new Transaction();
        transaction.setChild(child);
        transaction.setAmount(trans.getAmount());
        transaction.setParent(parent);
        transaction.setSchool(child.getSchool());
        transaction.setTransactionDate(new Date(new java.util.Date().getTime()));
        transaction.setParentAccount(parentAccount);

//        try{
//            transactionRepository.save(transaction);
//        }catch (Exception e){
//            returnResponse.put("Status", "Error");
//            returnResponse.put("cause", "An unexpected error occurred");
//            return ResponseEntity.badRequest().body(returnResponse);
//        }


        Data reqData = new Data();
        Data callBackData = new Data();
        ReceiveMoMoRequest req = new ReceiveMoMoRequest();
        req.setAmount(trans.getAmount());
        req.setCustomerEmail(parent.getEmail());
        req.setCustomerMsisdn(parentAccount.getAccountNumber());
        req.setChannel(parentAccount.getChannel());
        req.setDescription("School Fee Payment for "+child.getFirstName()+" "+child.getLastName()+" to "+child.getSchoolName());
        req.setPrimaryCallbackURL("https://webhook.site/29095693-bd93-42eb-b6ac-34a9326eea44");
        req.setCustomerName(parentAccount.getAccountName());
        req.setFeesOnCustomer(false);

        String reqJson = new Gson().toJson(req);

        try{
//            String response = new HttpWrapper()
//                    .MakeHubtelCalls(AppConstants.REQUEST_MOMO_URL, HttpWrapper.HttpMethods.POST, HttpWrapper.HttpContentType.ApplicationJson, reqJson);
//            requestResponse = new Gson().fromJson(response, ReceiveMoMoRequestResponse.class);
            transactionRepository.save(transaction);
        }catch (Exception e){
            returnResponse.put("Status", "Error");
            returnResponse.put("Reason", "Something unexpected happened. Please try again later");
            return ResponseEntity.badRequest().body(returnResponse);
        }

//        reqData = requestResponse.getData();
//        System.out.println("Request Data Description is: "+reqData.getDescription());


        returnResponse.put("status", "Saved");
        returnResponse.put("action", "Transaction has been saved");

        return ResponseEntity.ok(returnResponse);
    }

    /****************************************************
     *                      GET REQUESTS
     *****************************************************/

    @GetMapping("/getChildren")
    public @ResponseBody Collection<Child> getChildren(@RequestParam(name = "id") Long id){
        return childRepository.findAllByParentId(id);
    }

    @GetMapping("/getChildById")
    public @ResponseBody Child getChildById(@RequestParam(name = "childId") Long id){
        return childRepository.findOne(id);
    }

    @GetMapping("/getAllTransactions")
    public @ResponseBody Collection<Transaction> getTransactions(@RequestParam(name = "id") Long id){
        if(transactionRepository.findAll().isEmpty()){
            return null;
        }
        return transactionRepository.findAllByParentId(id);
    }

}
