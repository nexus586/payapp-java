package com.payapp.controllers;

import com.payapp.models.Message;
import com.payapp.repositories.MessageRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nexus on 3/18/18.
 */
@RequestMapping
@RestController
public class MessageController {

    private MessageRepository messageRepository;

    public MessageController(MessageRepository messageRepository){
        this.messageRepository = messageRepository;
    }

    @PostMapping(path = "/message/postNewMessage")
    public ResponseEntity<?> postMessage(@RequestBody Message message){
        Map<String, String> response = new HashMap<>();
        if(message == null){
            response.put("status", "Error");
            response.put("reason", "Your message could not be posted");
            return ResponseEntity.badRequest().body(response);
        }

        messageRepository.save(message);

        response.put("status", "Success");
        response.put("reason", "We appreciate your feedback. We will get back to you soon");
        return ResponseEntity.ok(response);
    }

//    @RolesAllowed("ADMIN")
    @GetMapping(path = "/getAllMessages")
    public Iterable<Message> getAllMessages(){
        return messageRepository.findAll();
    }
}
