package com.payapp.controllers;

import com.payapp.auth.details.JWTUser;
import com.payapp.auth.jwt.JwtTokenHelper;
import com.payapp.mail.MailManager;
import com.payapp.models.*;
import com.payapp.repositories.*;
import com.payapp.shared.Account;
import com.payapp.shared.AccountType;
import com.payapp.shared.Role;
import com.payapp.shared.RoleType;
import jdk.nashorn.internal.runtime.ECMAException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nexus on 12/17/17.
 */
@RestController
@RequestMapping(path = "/payapp/school")
public class SchoolController {

    private RoleRepository roleRepository;
    private TransactionRepository transactionRepository;
    private AccountRepository accountRepository;
    private UserRepository userRepository;
    private MailManager mailManager;
    private JwtTokenHelper helper;
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder encoder;

    public SchoolController(
                            RoleRepository roleRepository,
                            TransactionRepository transactionRepository,
                            AccountRepository accountRepository,
                            UserRepository userRepository,
                            MailManager mailManager,
			                UserDetailsService userDetailsService,
 			                JwtTokenHelper helper){
        this.roleRepository = roleRepository;
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.mailManager = mailManager;
	    this.userDetailsService = userDetailsService;
	    this.helper = helper;
    }



    //---------------------------- POST REQUESTS -------------------------------//

    @PostMapping("/signUp")
    public ResponseEntity<?> register(@RequestBody User school){

        User s = userRepository.findByEmail(school.getEmail());
        Collection<Role> r = new ArrayList<>();

        Role userRole = roleRepository.findById(4l);
        Role schoolRole = roleRepository.findById(3l);

        r.add(schoolRole);
        r.add(userRole);

        Map<String, Object> response = new HashMap<>();

        if(s == null){
//            try{
//                mailManager.setMessage("We are sending this email to confirm that your email is real " +
//                        "before we create your account");
//                mailManager.sendAccountCreationNotification(s);
//            }catch (Exception e){
//                response.put("status", "error");
//                response.put("reason", "We were unable to confirm your email.Please try again later");
//                return ResponseEntity.badRequest().body(response);
//            }

            s = new User();
            s.setEmail(school.getEmail());
            s.setName(school.getName());
            s.setPhone("0"+school.getPhone());
            s.setRoles(r);
            s.setEnabled(true);
            s.setLoggedIn(true);
            s.setPassword(encoder.encode(school.getPassword()));
            userRepository.save(s);
            response.put("status", "ok");
            response.put("reason", "Success");
            response.put("user", s);
        }else {
            response.put("status", "error");
            response.put("reason", "Unable to create Account");
            return ResponseEntity.badRequest().body(response);
        }

	    try{
            final JWTUser retUser = (JWTUser) userDetailsService.loadUserByUsername(school.getEmail());
            final String token = helper.generateToken(retUser);
            response.put("token", token);
            response.put("user", retUser.getId());
            response.put("name", retUser.getName());
            response.put("roles", retUser.getAuthorities());
        }catch (Exception e){
           response.put("status", "Error");
           response.put("reason", "Sorry An Unexpected Error Occurred");
        }


//        try{
//            mailManager.setMessage("Account has been successfully created. You can now use the full features of PayApp. " +
//                    "Thank you for signing up with us");
//            mailManager.sendAccountNotification(s);
//        }catch (Exception e){
//            response.put("status", "error");
//            response.put("reason", "Something bad happened");
//            return ResponseEntity.badRequest().body(response);
//        }

        return ResponseEntity.ok(response);
    }

    //---------------------- GET REQUESTS ----------------------------//


    @GetMapping("/viewAllTransactions")
    public Collection<Transaction> findAllMyTransactions(@RequestParam(name="id") Long id){
        if(transactionRepository.findAll().isEmpty()){
            return null;
        }
        return transactionRepository.findAllBySchoolId(id);
    }

    @GetMapping("viewTransaction")
    public Transaction findTransactionById(@RequestParam(name = "id") Long id){
        return transactionRepository.findOne(id);
    }
}
