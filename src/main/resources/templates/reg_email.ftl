<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link rel="script" href="../js/bootstrap.min.js"/>
    <title>Email Confirmation</title>
</head>
<body class="container">
    <div class="container">
        <h3>Dear ${user.name},</h3>
        <p class="card card-outline-primary">
            ${message}
        </p>
    </div>
    <footer class="modal-footer">
        <span>&copy;Copyright PayApp 2018</span>
    </footer>
</body>
</html>