<!Doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link rel="script" href="../js/bootstrap.min.js"/>
    <title>Account Confirmation</title>
</head>
<body class="container">
    <div class="container">
        <h2>Dear, ${user.name} </h2>
        <div>
            <p>
               ${message}
            </p>
        </div>
        <div class="modal-footer">
            <span>&copy;Copyright PayApp 2018</span>
        </div>
    </div>
</body>
</html>